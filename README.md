## The task

The goal is to count bits which are set to 1 in the array of numbers. This task is commonly known as [population count](https://en.wikichip.org/wiki/population_count). Here it's just a bit complicated by introducing an array (instead of a single number) - well, it doesn't so matter.

_Initially the task was defined for 8-bit numbers only. This fact (and attempt to go out of this bound) impacted the implementation, see the decription below._

## How to run

A common running pattern is:

  `node . <parameters>`

However you can just run:

  `node .`

and you will see instructions regarding `<parameters>`.


## Implementation notes

There are a few different implementations (differences are described below) of the program:

- for up to 8-bit numbers;
- for up to 32-bit numbers;
- for numbers of unlimited length.

To switch from one implementation to another change `require` in source code - at the top of `src/popcount-in-array.js` (I didn't want to complicate the program with automation of this).

_Pay attention: every implementation, in turn, has a few solutions..._


### For 8-bit numbers

The solution looked good enough when it was implemented for 8-bit numbers only (see `src/lib/popcount-in-array-of-8bit-integers.js`).

Particularly **`viaNodeBuffer` solution for this case looks compact and quasi-abstract (it may seem that `Buffer.from(arr)` works independently on the actual type of the arr's elements, while actually it's not so).**


### For 32-bit numbers

In the root of the algorythm used in the previous solution (see `calcPopCount` function in `src/lib/popcount32.js`) there is a bitwise right shift operator (`>>`). [The operator converts its operands to a 32-bit integer](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_Operators#Bitwise_shift_operators). Hence the `calcPopCount` function can process correctly only a number up to 32 bit.

_To operate on bigger numbers a more complex algorithm (or/and data types, operators, etc.) should be used._

So `lib/popcount-in-array-of-32bit-integers.js` is an extension of the previous 8-bit implementation to leverage `calcPopCount` function as much as possible (processing the user input by portions of 32 bits).

This implementation is basically almost the same as 8-bit one, but **pay attention how `viaNodeBuffer` solution has become less nice** (because of details of one more abstraction - this time it was NodeJS's "Buffer" implementation)...


### For numbers of arbitrary length

I didn't like pre-declaring/restricting size of numbers which could be entered, using size-bound types (like `Uint8Array`, `Uint32Array`), functions and operators, and I was seeking for a more universal/generic way.

I've found a solution (see `src/lib/popcount-in-array-of-any-integers.js`). It consists of the following steps:

1. Accept every passed array element (doesn't matter how long it is) into a **`BigInt`** instance;
2. Convert the value to its binary representation kept in a string _(with `n.toString(2)`, although it's not quite good - for example, for `-1` value, but I haven't found anything better...)_;
3. Count '1' symbols in the obtained strings:

    3.1. Either count '1' symbols in every obtained string (by removing all '0' symbols and then counting the remained '1' symbols, see "simple" solution);

    3.2 Or write all the obtained strings to a Stream and then count '1' characters while reading the stream (see "viaStream" solution);

    3.3. Or to combine 3.2 with 3.1: to read the stream by portions of some reasonable length and apply 3.1 to every portion.

The main achievement: A USER CAN ENTER AN ARRAY OF ARBITRARY (NOT LIMITED) DECIMAL VALUES!

"viaStream" solution could look an overhead. However, in a common case it could be better, because:
I can write the entered values to a stream and read the data from the stream with different velocity - i.e. by an arbitrary (convenient) chunks, regardless to which portions the data was actually written into the stream.

### The output of tests

```
Popcount calculation
    on up to 8-bit numbers
      in 8-bit implemenation
        ✓ is correctly calculated by "simple" solution
        ✓ is correctly calculated by "simpleWithCache" solution
        ✓ is correctly calculated by "viaNodeBuffer" solution
        ✓ is correctly calculated by "viaUint8Array" solution
      in 32-bit implemenation
        ✓ is correctly calculated by "simple" solution
        ✓ is correctly calculated by "simpleWithCache" solution
        ✓ is correctly calculated by "viaNodeBuffer" solution
        ✓ is correctly calculated by "viaInt32Array" solution
      in unlimited length implemenation
        ✓ is correctly calculated by "simple" solution
        ✓ is correctly calculated by "viaStream" solution
    on >8-bit and <32-bit numbers
      in 8-bit implemenation
        ✓ is WRONGLY calculated by "simple" solution
        ✓ is WRONGLY calculated by "simpleWithCache" solution
        ✓ is WRONGLY calculated by "viaNodeBuffer" solution
        ✓ is WRONGLY calculated by "viaUint8Array" solution
      in 32-bit implemenation
        ✓ is correctly calculated by "simple" solution
        ✓ is correctly calculated by "simpleWithCache" solution
        ✓ is correctly calculated by "viaNodeBuffer" solution
        ✓ is correctly calculated by "viaInt32Array" solution
      in unlimited length implemenation
        ✓ is correctly calculated by "simple" solution
        ✓ is correctly calculated by "viaStream" solution
    on numbers longer than 32 bit
      in 8-bit implemenation
        ✓ is WRONGLY calculated by "simple" solution
        ✓ is WRONGLY calculated by "simpleWithCache" solution
        ✓ is WRONGLY calculated by "viaNodeBuffer" solution
        ✓ is WRONGLY calculated by "viaUint8Array" solution
      in 32-bit implemenation
        ✓ is WRONGLY calculated by "simple" solution
        ✓ is WRONGLY calculated by "simpleWithCache" solution
        ✓ is WRONGLY calculated by "viaNodeBuffer" solution
        ✓ is WRONGLY calculated by "viaInt32Array" solution
      in unlimited length implemenation
        ✓ is correctly calculated by "simple" solution
        ✓ is correctly calculated by "viaStream" solution

  30 passing (24ms)
```
