'use strict';

const { calcPopCount, calcPopCountWithCaching } = require('./popcount32');

const { PassThrough } = require('stream');

// This program (due to use of >> operator) works correctly only with VALUES NOT BIGGER THAN 2147483647 (the max 32-bit binary).
// To operate on bigger numbers a more complex algorithm (or/and data types, operators, etc.) should be used.

const prompts = (msg) => {
  return "\nThe program processes only LOWER 4 BYTES of each array element (it works wrong with numbers bigger than 2147483647)!";
};

const solutions = {

  // The most straightforward solution - direct iterating an array taken from STDIN
  simple: (arr) => {
    return arr
      .map((e) => { return calcPopCount(e); })
      .reduce((res, cur) => { return res + cur; }, 0);
  },

  // The same solution as above, but with dynamic caching bit count for already encountered numbers
  simpleWithCache: (arr) => {
    return arr
      .map((e) => { return calcPopCountWithCaching(e); })
      .reduce((res, cur) => { return res + cur; }, 0);
  },

  // A solution via iterating Node.js Buffer
  viaNodeBuffer: (arr) => {
    // Oops, the Buffer class implements the Uint8Array API -
    // thus Buffer.from takes only one byte from every arr's element :(
    // const buffer = Buffer.from(arr);

    // So use of Node's Buffer looks not reasonable
    // (I need to explicitly calculate the buffer size
    // and do `buffer.writeInt32BE()` so that then I'd be able to do `buffer.readInt32BE()`).
    const buffer = Buffer.alloc(arr.length*4);
    arr.forEach((e, index) => {
      // I have to trim `e` to 32 bits - otherwise here will be a RangeError.
      // From https://nodejs.org/dist/latest-v12.x/docs/api/buffer.html#buffer_buf_writeint32be_value_offset:
      // value should be a valid signed 32-bit integer. Behavior is undefined when value is anything other than a signed 32-bit integer.
      const fourBytes   = 0xffffffff;
      buffer.writeInt32BE(e & fourBytes, index*4); // MANUAL "trimming" number to 4 bytes is done here (the rest is lost)
    });

    let count = 0;

    for (let n = 0; n < buffer.length; n++) {
      if(n % 4 === 0) {
        count += calcPopCountWithCaching(buffer.readInt32BE(n));
      }
    }

    return count;
  },

  // A solution via iterating ES6 Int32Array
  viaInt32Array: (arr) => {
    // A bit nicer than in `viaNodeBuffer` above, but still not very great,
    // because the argument is limited by 32 bits
    const byteArray = Int32Array.from(arr);  // "trimming" number to 4 bytes is done here (the rest is lost)
    let count = 0;

    byteArray.forEach((byte) => {
      count += calcPopCountWithCaching(byte);
    });

    return count;
  }

};

const parseArrayOfNumbers = (str) => {
  return JSON.parse(str);
};


module.exports = {
  solutions,
  prompts,
  parseArray: parseArrayOfNumbers
};
