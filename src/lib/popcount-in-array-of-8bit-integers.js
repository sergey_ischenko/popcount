'use strict';

const { calcPopCount, calcPopCountWithCaching } = require('./popcount32');

// This program (for the sake of simplicity) works correctly only with
// VALUES NOT BIGGER THAN 255 (1 BYTE).
// Any other number will be treated (trimmed etc.) as 8-bit unsigned integer (i.e. 348 -> 92).
// Extra notes:
// 1) Basically `calcPopCount` function processes correctly a number up to 2147483647 (the max 32-bit binary) - with 'simple' and 'simpleWithCache' implementations you can try that.
// 2) To operate on bigger numbers a more complex algorithm (or/and data types, operators, etc.) should be used.

const prompts = (msg) => {
  return "\nThe program processes only FIRST BYTE of each array element (it works wrong with numbers bigger than 255)!";
};

const oneByte   = 0xff;

const solutions = {

  // The most simple solution - iterating an array
  simple: (arr) => {
    return arr
      .map((e) => { return calcPopCount(e & oneByte); })  // explicit "trimming" number to 1 byte
      .reduce((res, cur) => { return res + cur; }, 0);
  },

  // The same solution, but with dynamic caching bit count for already encountered numbers
  simpleWithCache: (arr) => {
    return arr
      .map((e) => { return calcPopCountWithCaching(e & oneByte); }) // explicit "trimming" number to 1 byte
      .reduce((res, cur) => { return res + cur; }, 0);
  },

  // A solution via iterating Node.js Buffer
  viaNodeBuffer: (arr) => {
    const buffer = Buffer.from(arr); // AUTOMATIC "trimming" number to 1 byte is done here
    let count = 0;

    for (let n = 0; n < buffer.length; n++) {
      count += calcPopCountWithCaching(buffer.readUInt8(n));
    }

    return count;
  },

  // A similar solution via iterating ES6 Uint8Array
  viaUint8Array: (arr) => {
    const byteArray = Uint8Array.from(arr);  // "trimming" number to 1 byte is done here
    let count = 0;

    byteArray.forEach((byte) => {
      count += calcPopCountWithCaching(byte);
    });

    return count;
  }

};

const parseArrayOfNumbers = (str) => {
  return JSON.parse(str);
};


module.exports = {
  solutions,
  prompts,
  parseArray: parseArrayOfNumbers
};
