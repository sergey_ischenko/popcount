const { PassThrough } = require('stream');

const prompts = (msg) => {
  return "\nThe program processes integers of any length!\nYou can pass such an array, for example: [348,123456789012345678901234567890]";
};

const solutions = {

  // The most simple solution - iterating an array of strings,
  // leaving only '1' symbols in each string and then counting the resulting string length.
  simple: (arr) => {
    return arr
      .map((e) => { return e.toString(2).replace(/0/g, '').length; })
      .reduce((res, cur) => { return res + cur; }, 0);
  },

  // Likely not very efficient (by memory usage and performance),
  // but functionally great
  viaStream: (arr) => {
    return new Promise((resolve) => {
      let popCount = 0;

      const passthrough = new PassThrough({ encoding: 'utf8' });

      passthrough.on('readable', () => {
        let chunk;
        while (null !== (chunk = passthrough.read(1))) {  // read by 1 character
          if(chunk == '1') {
            popCount++;
          }
        }
        // Notes about this solution:
        // It was basically possible to read from passthrough by 32 bits at once and leverage popcount32.js,
        // but it doesn't look reasonable (at least would look more weird).
        // In a real-life task, maybe it would make sense in favor of performance, I am not sure.
      });

      passthrough.on('end', () => {
        resolve(popCount);
      });

      arr.forEach((s) => {
        // write the stringified values into the stream, NOT KNOWING THEIR ACTUAL LENGTH
        // (in a real life it would make sense to write values into a stream ASAP,
        // not putting them into an intermediate array (arr) first)
        passthrough.write(s.toString(2));
      });
      passthrough.end();

    });
  }

};

const parseArrayOfLongIntegers = (str) => {
  // Stupid manual parsing of the entered array, because JSON.parse doesn't work with BigInt...
  return str
    .replace(/\[|\]|\s+/g, '')
    .split(',')
    .map((v) => { return BigInt(e); });  // BigInt allows to process a very long value
}


module.exports = {
  solutions,
  prompts,
  parseArray: parseArrayOfLongIntegers
};
