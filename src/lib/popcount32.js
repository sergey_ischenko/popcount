// Count 1s in a number up to 2147483647 (the max 32-bit binary) -
// this limitation is caused by `>>` operator.
// To operate on bigger numbers a more complex algorythm (or/and data types, operators, etc.) should be used.
const calcPopCount = (n) => {
  let count = 0;
  while (n > 0) {
    count += n & 1;
    n >>= 1; // this operation limits n to the max 32-bit binary
  }
  return count;
};

// Caution! This cache is shared between all clients of this module during the same execution session.
// It's not so matter in this task, but in a common case in should be taken into account...
const cache = {};

const calcPopCountWithCaching = (n) => {
  return cache[n] || (cache[n] = calcPopCount(n));
};

module.exports = { calcPopCount, calcPopCountWithCaching };
