const { solutions, parseArray, prompts} =
  // require('./lib/popcount-in-array-of-8bit-integers');
  require('./lib/popcount-in-array-of-32bit-integers');
  // require('./lib/popcount-in-array-of-any-integers');

const getSolution = (solutions, chosenSolution, help) => {
  if (solutions[chosenSolution]) {
    return solutions[chosenSolution];
  } else {
    help(`Solution '${chosenSolution}' is not implemented.`);
  }
};

const help = (msg) => {
  if (msg) {
    console.info(msg);
  }

  console.info('\nExample of use:\n\tnode popcount-in-array.js <solution> [1,7,255]');
  console.info(`\nImplemented solutions: ${Object.keys(solutions)}`);
  console.info(prompts());
};

const main = async () => {
  if(process.argv.length < 4) { // node popcount-in-array.js simple [1,2,200]
    help();
    return;
  }

  let chosenSolution = process.argv[2];
  let sArray = process.argv[3];

  try {
    array = parseArray(sArray);
  } catch (e) {
    throw new Error(`Error on parsing the passed array: ${e}`);
  }

  let solution;
  if(solution = getSolution(solutions, chosenSolution, help)) {
    const popCount = await solution(array); // `await` here allows `solution` to be either sync or async.
    console.info(`\nThere are ${popCount} bits totally set in numbers of this array: ${array}.`);
  }
};

main();
