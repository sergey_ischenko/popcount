const chai = require("chai");
chai.use(require("sinon-chai"));
chai.use(require("chai-as-promised"));

const helpers = {};

// Stuff `helpers` with anything you want to share between test suites

require("./sinon.sandbox")(helpers);
require("./paths")(helpers);
// require("./factory")(helpers);

module.exports = helpers;
