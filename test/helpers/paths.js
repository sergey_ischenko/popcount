const path = require("path");

const root = path.resolve(__dirname, "../..");
const fromRoot = function (relativePath) {
  return path.join(root, relativePath);
};

module.exports = function (helpers) {
  helpers.paths = { root, fromRoot };
}
