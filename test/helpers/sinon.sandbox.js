const sinon = require("sinon");

module.exports = function (helpers) {
// Call this function at the top of every context, where you want to use a Sinon Sandbox.
// The sandbox will be accessible via `helpers.sandbox`
// in any children it/beforeEach/afterEach (not in before/after/describe/context)
  helpers.setupSinonSandbox = function () {
    beforeEach(() => {
      helpers.sandbox = sinon.createSandbox();

      // Shortcuts
      helpers.mock = helpers.sandbox.mock.bind(helpers.sandbox);
      helpers.stub = helpers.sandbox.stub.bind(helpers.sandbox);
      helpers.spy = helpers.sandbox.spy.bind(helpers.sandbox);
    });

    afterEach(() => {
      helpers.sandbox.verifyAndRestore();

      // Shortcuts
      delete helpers.mock;
      delete helpers.stub;
      delete helpers.spy;
    });
  };

  helpers.setupSinonSandbox();
};
