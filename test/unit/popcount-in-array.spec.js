const { expect }  = require("chai");

const helpers = require("../helpers");

const popcount8bit = require(helpers.paths.fromRoot("src/lib/popcount-in-array-of-8bit-integers"));
const popcount32bit = require(helpers.paths.fromRoot("src/lib/popcount-in-array-of-32bit-integers"));
const popcountUnlimited = require(helpers.paths.fromRoot("src/lib/popcount-in-array-of-any-integers"));

describe("Popcount calculation", () => {

  describe("on up to 8-bit numbers", () => {
    const array = [5,255];
    // > Number(255).toString(2)
    // '11111111'

    describe("in 8-bit implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcount8bit.solutions)) {
        it(`is correctly calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(10);
        });
      }
    });

    describe("in 32-bit implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcount32bit.solutions)) {
        it(`is correctly calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(10);
        });
      }
    });

    describe("in unlimited length implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcountUnlimited.solutions)) {
        it(`is correctly calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(10);
        });
      }
    });
  });

  describe("on >8-bit and <32-bit numbers", () => {
    const array = [5,2147483647];
    // > Number(2147483647).toString(2)
    // '1111111111111111111111111111111'

    describe("in 8-bit implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcount8bit.solutions)) {
        it(`is WRONGLY calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(10);
        });
      }
    });

    describe("in 32-bit implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcount32bit.solutions)) {
        it(`is correctly calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(33);
        });
      }
    });

    describe("in unlimited length implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcountUnlimited.solutions)) {
        it(`is correctly calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(33);
        });
      }
    });
  });

  describe("on numbers longer than 32 bit", () => {
    const array = [5,123456789012345678901234567890];
    // > BigInt(123456789012345678901234567890).toString(2)
    // '1100011101110100100001111111101101100001101110011111000000000000000000000000000000000000000000000'

    describe("in 8-bit implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcount8bit.solutions)) {
        it(`is WRONGLY calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(2);
        });
      }
    });

    describe("in 32-bit implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcount32bit.solutions)) {
        it(`is WRONGLY calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(2);
        });
      }
    });

    describe("in unlimited length implemenation", async () => {
      for (let [solName, solImpl] of Object.entries(popcountUnlimited.solutions)) {
        it(`is correctly calculated by "${solName}" solution`, async () => {
          expect(await solImpl(array)).to.be.equal(34);
        });
      }
    });
  });

});
